//
//  AuthorizationOperation.swift
//  WDFoundation
//
//  Created by Jorge Galrito on 15/04/2018.
//  Copyright © 2018 WATERDOG. All rights reserved.
//

import Foundation
import WDOperations
import os.log

public enum AuthenticationError: LocalizedError {
  case suspensionError(String)
  case hardError(String)
  
  public var errorDescription: String? {
    switch self {
    case let .suspensionError(message):
      return message
    case let .hardError(message):
      return message
    }
  }
}

public protocol AuthenticationRemoteService {
  func login(with credentials: UserCredentials) -> URLRequest
  func logout(token: String) -> URLRequest
  func error(fromStatusCode statusCode: Int, data: Data) -> AuthenticationError?
}

private class AuthorizationOperationQueue {
  fileprivate static let showLoginOperationQueue = WDOperationQueue()
  fileprivate static let showLoginQueue = DispatchQueue(label: "ShowLoginOperationQueue")
  fileprivate static var authorizationOperation: AnyAuthorizationOperation?
}

public class AnyAuthorizationOperation: WDOperation {
}

public class AuthorizationOperation<Access: UserAccess>: AnyAuthorizationOperation {
  let remoteService: AuthenticationRemoteService
  
  public var authorizationResultHandlers: [(Access?) -> Void] = []
  
  private let internalQueue: WDOperationQueue
  private let loginOperation: PromptCredentialsOperation
  private let session: URLSession
  
  public init(remoteService: AuthenticationRemoteService, session: URLSession = .shared, loginOperation: PromptCredentialsOperation, queue: WDOperationQueue = WDOperationQueue()) {
    self.remoteService = remoteService
    self.session = session
    self.internalQueue = queue
    self.loginOperation = loginOperation
    if #available(iOS 10, *) {
      os_log("Initializing AuthorizationOperation", log: authenticationLog, type: .debug)
    }
  }
  
  open override func execute() {
    if #available(iOS 10, *) {
      os_log("AuthorizationOperation start execution", log: authenticationLog, type: .info)
    }
    
    if #available(iOS 10.0, *) {
      os_log("Check if an authorization token is already present", log: authenticationLog, type: .debug)
    }
    let controller = AuthorizationController<Access>()
    if let userAccess = controller.userAccess {
      if #available(iOS 10, *) {
        os_log("Authorization token retrieved: \"%@\"", log: authenticationLog, type: .info, userAccess.authorizationToken)
      }
      self.callHandlers(with: userAccess)
      return
    }
    
    if let (username, password) = readPasswordFromKeychain() {
      let credentials = UserCredentials(username: username, password: password)
      let operation = AuthenticationOperation<Access>(
        remote: remoteService,
        credentials: credentials,
        session: session) { authorization, _ in
          guard let authorization = authorization else {
            self.queueShowLoginOperation()
            return
          }
          
          AuthorizationController().store(userAccess: authorization)
          
          self.callHandlers(with: authorization)
      }
      internalQueue.addOperation(operation)
    } else {
      queueShowLoginOperation()
    }
  }
  
  private func queueShowLoginOperation() {
    loginOperation.handler = { credentials, callback in
      let operation = AuthenticationOperation<Access>(
        remote: self.remoteService,
        credentials: credentials,
        session: self.session) { authorization, error in
          guard let authorization = authorization else {
            callback(.error(error ?? .hardError("Error logging in")))
            return
          }
          
          AuthorizationController().store(userAccess: authorization)
          
          self.callHandlers(with: authorization)
          callback(.success)
      }
      self.internalQueue.addOperation(operation)
    }
    internalQueue.addOperation(loginOperation)
  }
  
  private func callHandlers(with userAccess: Access?) {
    authorizationResultHandlers.forEach { $0(userAccess) }
    finish()
  }
  
  private func readPasswordFromKeychain() -> (String, String)? {
    if #available(iOS 10, *) {
      os_log("Reach Keychain for credentials", log: authenticationLog, type: .info)
    }
    
    let bundleIdentifier = Bundle.main.bundleIdentifier!
    let credentialsQuery: [String: AnyObject] = [
      kSecClass as String: kSecClassGenericPassword,
      kSecAttrService as String: bundleIdentifier as AnyObject,
      kSecReturnData as String: kCFBooleanTrue,
      kSecMatchLimit as String: kSecMatchLimitOne,
      kSecReturnAttributes as String: kCFBooleanTrue
    ]
    if #available(iOS 10, *) {
      os_log("-- Keychain query: %{public}@", log: authenticationLog, type: .debug, credentialsQuery)
    }
    
    var storedItem: AnyObject?
    let result = withUnsafeMutablePointer(to: &storedItem) { SecItemCopyMatching(credentialsQuery as CFDictionary, UnsafeMutablePointer($0)) }
    
    guard result == errSecSuccess else {
      if #available(iOS 10, *) {
        os_log("-- No credentials found on keychain", log: authenticationLog, type: .debug)
      }
      return nil
    }
    
    let credentials: (String, String)?
    if let result = storedItem as? [String: AnyObject],
      let username = result[kSecAttrAccount as String] as? String,
      let encodedPassword = result[kSecValueData as String] as? Data,
      let password = String(data: encodedPassword, encoding: .utf8) {
      credentials = (username, password)
      if #available(iOS 10, *) {
        os_log("-- Credentials extracted with success: %{private}@, %{private}@", log: authenticationLog, type: .info, username, password)
      }
    } else {
      credentials = nil
      if #available(iOS 10, *) {
        os_log("-- Credentials could not be extracted from the result", log: authenticationLog, type: .error)
      }
    }
    
    return credentials
  }

}
